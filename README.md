CMAKE helper file for win-cpp-deps c++ package manager for Windows.

After including this file, you can add dependancies with the following code:

install_dep("https://bitbucket.org/wincppdeps/glfw.git")

This will clone, configure, build and install the git repository behind the URL in your user directory "%USERPROFILE%\.win-cpp-dep". Every cmake generator will gets its own subdirecory in this home directory. The root of this directory can be changed by setting the environment variable "WINCPPDEP_HOME". The CMAKE_PREFIX_PATH will be changed to "<WIN_CPP_DEPS_HOME>\<GENERATOR_NAME>\install".

For this package manager to work, you will need a working cmake and git installation.
