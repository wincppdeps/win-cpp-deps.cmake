if(NOT WIN32)
	message(STATUS This is no windows environment so win-cpp-dep is not needed here)

	function(INSTALL_DEP URL)

	endfunction(INSTALL_DEP)

	return ()
endif()
if(EXISTS "$ENV{WINCPPDEP_HOME}")
    set(WINCPPDEP_FOLDER "$ENV{WINCPPDEP_HOME}\\${CMAKE_GENERATOR}")
else()
    set(WINCPPDEP_FOLDER "$ENV{USERPROFILE}\\.win-cpp-dep\\${CMAKE_GENERATOR}")
endif()

set(WINCPPDEP_SRC_FOLDER "${WINCPPDEP_FOLDER}\\src")
set(WINCPPDEP_BUILD_FOLDER "${WINCPPDEP_FOLDER}\\build")
set(WINCPPDEP_INSTALL_FOLDER "${WINCPPDEP_FOLDER}\\install")

set(CMAKE_PREFIX_PATH "${WINCPPDEP_INSTALL_FOLDER}")
set(CMAKE_MODULE_PATH "${WINCPPDEP_INSTALL_FOLDER}\\cmake-modules")

file(MAKE_DIRECTORY ${WINCPPDEP_FOLDER})
file(MAKE_DIRECTORY ${WINCPPDEP_SRC_FOLDER})
file(MAKE_DIRECTORY ${WINCPPDEP_BUILD_FOLDER})
file(MAKE_DIRECTORY ${WINCPPDEP_INSTALL_FOLDER})

function(INSTALL_DEP URL)

	get_filename_component(REPO_NAME ${URL} NAME)

	if (EXISTS "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		message(STATUS "${REPO_NAME} is already build, so skipping for now")
		return()
	endif()

	file(MAKE_DIRECTORY "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}")

	message(STATUS "Installing ${REPO_NAME}...")

	if (EXISTS "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		if (EXISTS "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}\\.git")
			set(git_status_cmd "git" "status")
			execute_process(COMMAND ${git_status_cmd}
				WORKING_DIRECTORY "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}"
				RESULT_VARIABLE git_status_result
				OUTPUT_VARIABLE git_status_stdout)
		else()
			set(git_status_result 1) # trigger a clone
		endif()
	else()
		file(MAKE_DIRECTORY "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		set(git_status_result 1) # trigger a clone
	endif()

	if (NOT ${git_status_result} EQUAL 0)
		file(MAKE_DIRECTORY "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		# Cloning the repo from URL
		message(STATUS "    Cloning...")
		set(git_clone_cmd "git" "clone" "--recurse-submodules" "${URL}" "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		execute_process(COMMAND ${git_clone_cmd}
			WORKING_DIRECTORY "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}"
			RESULT_VARIABLE git_result
			OUTPUT_VARIABLE git_stdout)
	else()
		# Pulling the repo from URL
		message(STATUS "    Pulling...")
		set(git_fetch_cmd "git" "pull")
		execute_process(COMMAND ${git_fetch_cmd}
			WORKING_DIRECTORY "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}"
			RESULT_VARIABLE git_result
			OUTPUT_VARIABLE git_stdout)
	endif()

	if(NOT ${git_result} EQUAL 0)
		message(STATUS )
		message(STATUS "ERROR cloning or fetching ${REPO_NAME}.")
		message(STATUS "Could you please empty ${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
		message(STATUS )

		# When fetching failed, we quit
		return()
	else()
		message(STATUS "    done")
	endif()

	# Generate project files with cmake
	message(STATUS "    Generated...")
	set(cmake_gen_cmd "cmake" "-G" "${CMAKE_GENERATOR}" "-DCMAKE_INSTALL_PREFIX=${WINCPPDEP_INSTALL_FOLDER}" "${WINCPPDEP_SRC_FOLDER}\\${REPO_NAME}")
	execute_process(COMMAND ${cmake_gen_cmd}
		WORKING_DIRECTORY "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}"
		RESULT_VARIABLE cmake_gen_result
		OUTPUT_VARIABLE cmake_gen_stdout)

	if(NOT ${cmake_gen_result} EQUAL 0)
		message(STATUS )
		message(STATUS "ERROR generating ${REPO_NAME} with cmake.")
		message(STATUS )

		# When generating failed, we quit
		return()
	else()
		message(STATUS "    done")
	endif()

	# Build the project
	message(STATUS "    Building...")
	set(cmake_build_cmd "cmake" "--build" "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}" "--target" "all")
	execute_process(COMMAND ${cmake_build_cmd}
		WORKING_DIRECTORY "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}"
		RESULT_VARIABLE cmake_build_result
		OUTPUT_VARIABLE cmake_build_stdout)

	if(NOT ${cmake_build_result} EQUAL 0)
		message(STATUS )
		message(STATUS "ERROR building ${REPO_NAME} with cmake.")
		message(STATUS )

		# When building failed, we quit
		return()
	else()
		message(STATUS "    done")
	endif()

	# Install the project
	message(STATUS "    Installing...")
	set(cmake_install_cmd "cmake" "--build" "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}" "--target" "install")
	execute_process(COMMAND ${cmake_install_cmd}
		WORKING_DIRECTORY "${WINCPPDEP_BUILD_FOLDER}\\${REPO_NAME}"
		RESULT_VARIABLE cmake_install_result
		OUTPUT_VARIABLE cmake_install_stdout)

	if(NOT ${cmake_install_result} EQUAL 0)
		message(STATUS )
		message(STATUS "ERROR installing ${REPO_NAME} with cmake.")
		message(STATUS )

		# When installing failed, we quit
		return()
	else()
		message(STATUS "    done")
	endif()
	message(STATUS "${REPO_NAME} is installed.")
	message(STATUS ----------------------------------------)

endfunction(INSTALL_DEP)
